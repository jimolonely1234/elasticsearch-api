package com.jimo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregator;
import org.elasticsearch.search.aggregations.bucket.global.Global;
import org.elasticsearch.search.aggregations.bucket.global.GlobalAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.missing.Missing;
import org.elasticsearch.search.aggregations.bucket.missing.MissingAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.bucket.range.date.DateRangeAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.avg.AvgAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.cardinality.Cardinality;
import org.elasticsearch.search.aggregations.metrics.cardinality.CardinalityAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.min.Min;
import org.elasticsearch.search.aggregations.metrics.min.MinAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.percentiles.Percentile;
import org.elasticsearch.search.aggregations.metrics.percentiles.Percentiles;
import org.elasticsearch.search.aggregations.metrics.percentiles.PercentilesAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.stats.Stats;
import org.elasticsearch.search.aggregations.metrics.stats.StatsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.tophits.TopHits;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCount;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCountAggregationBuilder;
import org.junit.Before;
import org.junit.Test;

import java.net.UnknownHostException;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders.randomFunction;

/**
 * @author wangpeng
 * @func Query的查询方式
 * @date 2018/8/12 19:31
 */
public class QueryUsage {

    private final Logger logger = LogManager.getLogger(QueryUsage.class);

    private TransportClient client;

    private String indexName = "20180801-openstack_log";
    private String type = "openstack_log";


    /**
     * @func 初始化连接
     * @author wangpeng
     * @date 2018/8/11 21:56
     */
    @Before
    public void init() {
        try {
            client = ESAPI.getTransportClient();
        } catch (UnknownHostException e) {
            logger.error("初始化失败");
        }
    }

    /**
     * @func 负责打印返回的结果
     * @author wangpeng
     * @date 2018/8/12 19:48
     */
    private void printSearchResponse(SearchResponse response) {
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * @func 查询所有，限制返回2条结果
     * @author wangpeng
     * @date 2018/8/12 19:37
     */
    @Test
    public void matchAll() {
        MatchAllQueryBuilder queryBuilder = matchAllQuery();
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(2)
                .get();
        printSearchResponse(response);
    }

    /**
     * @func 查出log日志message中含有ERROR的doc
     * @author wangpeng
     * @date 2018/8/12 19:48
     */
    @Test
    public void match() {
        MatchQueryBuilder queryBuilder =
                matchQuery("message", "ERROR");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 在 多个字段里寻找文本
     * @author wangpeng
     * @date 2018/8/12 19:52
     */
    @Test
    public void multiMatch() {
        MultiMatchQueryBuilder queryBuilder = multiMatchQuery(
                "SchedulerManager ERROR",
                "message", "logLevel"
        );
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }


    /**
     * @func
     * @author wangpeng
     * @date 2018/8/16 15:46
     */
    @Test
    public void multiSearch() {
        SearchRequestBuilder srb1 = client
                .prepareSearch()
                .setQuery(QueryBuilders.queryStringQuery("elasticsearch"))
                .setSize(1);
        SearchRequestBuilder srb2 = client
                .prepareSearch()
                .setQuery(QueryBuilders.matchQuery("name", "kimchy"))
                .setSize(1);

        MultiSearchResponse sr = client.prepareMultiSearch()
                .add(srb1)
                .add(srb2)
                .get();

        long nbHits = 0;
        for (MultiSearchResponse.Item item : sr.getResponses()) {
            SearchResponse response = item.getResponse();
            nbHits += response.getHits().getTotalHits();
        }
    }

    /**
     * @func 一般术语查询
     * @author wangpeng
     * @date 2018/8/12 20:06
     */
    @Test
    public void commonTerms() {
        CommonTermsQueryBuilder queryBuilder = commonTermsQuery("message", "ERROR");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 查询包括ERROR但没有Unauthorized的记录
     * @author wangpeng
     * @date 2018/8/12 20:08
     */
    @Test
    public void queryString() {
        QueryStringQueryBuilder queryBuilder = queryStringQuery("+ERROR -Unauthorized");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 查询包括ERROR但没有Unauthorized的记录
     * @author wangpeng
     * @date 2018/8/12 20:17
     */
    @Test
    public void simpleQueryString() {
        SimpleQueryStringBuilder queryBuilder = simpleQueryStringQuery("+ERROR -Unauthorized");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 指定术语查询
     * @author wangpeng
     * @date 2018/8/12 20:21
     */
    @Test
    public void termQueryTest() {
        TermQueryBuilder queryBuilder = termQuery("hostname", "sr-controller-2");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 查询hostname为sr-controller-2或sr-controller-3的
     * @author wangpeng
     * @date 2018/8/12 20:25
     */
    @Test
    public void termsQueryTest() {
        TermsQueryBuilder queryBuilder = termsQuery(
                "hostname", "sr-controller-2", "sr-controller-3");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 查询日期范围
     * @author wangpeng
     * @date 2018/8/12 20:34
     */
    @Test
    public void rangeQueryTest() {
        RangeQueryBuilder queryBuilder = rangeQuery("logdate")
                .from("2018-08-01 17:48:40.398", true)
                .to("2018-08-01 18:50:40.398", false);
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 查询exceptionRegTag不为空的记录
     * @author wangpeng
     * @date 2018/8/12 20:37
     */
    @Test
    public void existsQueryTest() {
        ExistsQueryBuilder queryBuilder = existsQuery("exceptionRegTag");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 匹配hostname前缀为sr-controller的记录
     * @author wangpeng
     * @date 2018/8/12 20:45
     */
    @Test
    public void prefixQueryTest() {
        PrefixQueryBuilder queryBuilder = prefixQuery("hostname", "sr-controller");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 匹配通配符
     * @author wangpeng
     * @date 2018/8/12 20:50
     */
    @Test
    public void wildcardTest() {
        WildcardQueryBuilder queryBuilder = wildcardQuery("hostname", "sr-*-3");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }


    /**
     * @func 正则查询
     * @author wangpeng
     * @date 2018/8/12 20:54
     */
    @Test
    public void regexpTest() {
        RegexpQueryBuilder queryBuilder = regexpQuery("hostname", "sr-.*-2");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 模糊查询
     * @author wangpeng
     * @date 2018/8/12 21:49
     */
    @Test
    public void fuzzyQueryTest() {
        FuzzyQueryBuilder queryBuilder = fuzzyQuery("module", "nova");
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 查询type为openstack_log的记录
     * @author wangpeng
     * @date 2018/8/12 20:56
     */
    @Test
    public void typeQueryTest() {
        TypeQueryBuilder queryBuilder = typeQuery("openstack_log");
        SearchResponse response = client.prepareSearch(indexName)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func Id 查询
     * @author wangpeng
     * @date 2018/8/12 21:00
     */
    @Test
    public void idsQueryTest() {
        IdsQueryBuilder queryBuilder = idsQuery(type)
                .addIds("AWT05OL8BdYcttDw9y7t", "AWT05OL8BdYcttDw9y8p");
        SearchResponse response = client.prepareSearch(indexName)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 给下面module匹配为nova的记录都给出相同的分数2.0
     * @author wangpeng
     * @date 2018/8/12 21:08
     */
    @Test
    public void constantScoreQueryTest() {
        ConstantScoreQueryBuilder queryBuilder = constantScoreQuery(
                termQuery("module", "nova")
        ).boost(2.0f);
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func bool条件符合查询
     * @author wangpeng
     * @date 2018/8/12 21:15
     */
    @Test
    public void boolQueryTest() {
        BoolQueryBuilder queryBuilder = boolQuery()
                .must(termQuery("module", "nova"))
                .mustNot(termQuery("hostname", "sr-controller-1"))
                .should(termQuery("logLevel", "DEBUG"))
                .filter(matchAllQuery());
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 不知道干嘛的
     * @author wangpeng
     * @date 2018/8/12 21:27
     */
    @Test
    public void disMaxQueryTest() {
        DisMaxQueryBuilder queryBuilder = disMaxQuery()
                .add(termQuery("module", "nova"))
                .add(termQuery("module", "cinder"))
                .boost(1.2f)
                .tieBreaker(0.7f);
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 随机分数定义
     * @author wangpeng
     * @date 2018/8/12 21:34
     */
    @Test
    public void functionScoreTest() {
        FunctionScoreQueryBuilder.FilterFunctionBuilder[] functions =
                {
                        new FunctionScoreQueryBuilder.FilterFunctionBuilder(
                                matchQuery("module", "nova"),
                                randomFunction("seed")//随机种子随机生成分数
                        )
                };
        FunctionScoreQueryBuilder queryBuilder = QueryBuilders.functionScoreQuery(functions);
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func 提升nova，降低cinder分数
     * @author wangpeng
     * @date 2018/8/13 8:48
     */
    @Test
    public void boostingQueryTest() {
        BoostingQueryBuilder queryBuilder = boostingQuery(
                termQuery("module", "nova"),
                termQuery("module", "cinder")
        ).negativeBoost(0.2f);
        SearchResponse response = client.prepareSearch(indexName).setTypes(type)
                .setQuery(queryBuilder)
                .setSize(10).get();
        printSearchResponse(response);
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/13 8:53
     */
    @Test
    public void indicesQueryTest() {
    }

    /**
     * @func 嵌套查询：先按模块分组，然后按日期的年份分，最后统计logLevel的数量
     * @author wangpeng
     * @date 2018/8/13 9:12
     */
    @Test
    public void structureAggs() {
        SearchResponse response = client.prepareSearch(indexName)
                .addAggregation(
                        AggregationBuilders.terms("by_module").field("module")
                                .subAggregation(
                                        AggregationBuilders.dateHistogram("by_year")
                                                .field("logdate")
                                                .dateHistogramInterval(DateHistogramInterval.YEAR)
                                                .subAggregation(
                                                        AggregationBuilders.count("count_logLevel").field("logLevel")
                                                )
                                )
                ).execute().actionGet();
        Terms byModule = response.getAggregations().get("by_module");
        for (Terms.Bucket entry : byModule.getBuckets()) {
            String key = entry.getKeyAsString();
            long docCount = entry.getDocCount();
            System.out.println("key: " + key + ",docCount:" + docCount);

            Histogram byYear = entry.getAggregations().get("by_year");
            for (Histogram.Bucket b : byYear.getBuckets()) {
                String k = b.getKeyAsString();
                System.out.println("k=" + k);

                ValueCount cnt = b.getAggregations().get("count_logLevel");
                System.out.println("cnt=" + cnt);
            }
        }
        /*
        key: nova,docCount:15788
        k=2018-01-01 00:00:00.000
        cnt=count[15788]
        key: neutron,docCount:9749
        k=2018-01-01 00:00:00.000
        cnt=count[9749]
        key: keystone,docCount:7356
        k=2018-01-01 00:00:00.000
        cnt=count[7356]
        key: cinder,docCount:3692
        k=2018-01-01 00:00:00.000
        cnt=count[3692]
        key: messages,docCount:3201
        k=2018-01-01 00:00:00.000
        cnt=count[3201]
        key: dashboard,docCount:947
        k=2018-01-01 00:00:00.000
        cnt=count[947]
        key: glance,docCount:351
        k=2018-01-01 00:00:00.000
        cnt=count[351]
        key: rabbitmq,docCount:168
        k=2018-01-01 00:00:00.000
        cnt=count[168]
        key: keepalived,docCount:12
        k=2018-01-01 00:00:00.000
        cnt=count[12]
        * */
    }

    /**
     * @func 需要double类型的字段
     * @author wangpeng
     * @date 2018/8/13 9:43
     */
    @Test
    public void minAgg() {
        MinAggregationBuilder builder = AggregationBuilders.min("size_min").field("alarmnum");
        SearchResponse response = client.prepareSearch("201807-metric_alarm_data")
                .addAggregation(builder).execute().actionGet();
        Min min = response.getAggregations().get("size_min");
        double minValue = min.getValue();
        System.out.println(minValue);
    }

    @Test
    public void maxAgg() {

    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/13 9:54
     */
    @Test
    public void avgAgg() {
        AvgAggregationBuilder builder = AggregationBuilders.avg("agg").field("alarmnum");
        SearchResponse response = client.prepareSearch("201807-metric_alarm_data")
                .addAggregation(builder).execute().actionGet();
        Avg min = response.getAggregations().get("agg");
        double minValue = min.getValue();
        System.out.println(minValue);
    }

    /**
     * @func 统计信息
     * @author wangpeng
     * @date 2018/8/13 11:43
     */
    @Test
    public void statsAgg() {
        //TODO 等待有double字段
        StatsAggregationBuilder agg = AggregationBuilders.stats("agg").field("");
        SearchResponse response = client.prepareSearch(indexName)
                .addAggregation(agg).execute().actionGet();
        Stats stats = response.getAggregations().get("agg");
        double max = stats.getMax();
        double min = stats.getMin();
        double avg = stats.getAvg();
        long count = stats.getCount();
        double sum = stats.getSum();
    }

    /**
     * @func 计算module的数量
     * @author wangpeng
     * @date 2018/8/13 9:57
     */
    @Test
    public void valueCount() {
        ValueCountAggregationBuilder builder = AggregationBuilders.count("cnt_module").field("module");
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        ValueCount cntModule = response.getAggregations().get("cnt_module");
        System.out.println(cntModule.getValue());//41264
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/13 11:50
     */
    @Test
    public void percentileAgg() {
        //TODO double
        PercentilesAggregationBuilder builder = AggregationBuilders.percentiles("agg")
                .field("")
                .percentiles(1.0, 5.0, 10.0, 20.0, 30.0, 75.0, 95.0, 99.0);//自定义百分比
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Percentiles agg = response.getAggregations().get("agg");
        for (Percentile p : agg) {
            p.getPercent();
            p.getValue();
        }
    }

    /**
     * @func 查询有几种module
     * @author wangpeng
     * @date 2018/8/13 11:58
     */
    @Test
    public void cardinalityAgg() {
        CardinalityAggregationBuilder builder = AggregationBuilders.cardinality("agg").field("module");
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Cardinality agg = response.getAggregations().get("agg");
        System.out.println(agg.getValue());//9
    }

    /**
     * @func 经纬度边界计算
     * @author wangpeng
     * @date 2018/8/13 12:02
     */
    @Test
    public void geoBoundsAgg() {

    }

    /**
     * @func 按module聚合并取top hit
     * @author wangpeng
     * @date 2018/8/13 12:34
     */
    @Test
    public void topHitsAgg() {
        TermsAggregationBuilder builder = AggregationBuilders.terms("agg_module").field("module")
                .subAggregation(
                        AggregationBuilders.topHits("top")
                                .from(2)
                                .size(20)
//                        .sort();
                );
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Terms terms = response.getAggregations().get("agg_module");
        for (Terms.Bucket bucket : terms.getBuckets()) {
            String key = bucket.getKeyAsString();
            long docCount = bucket.getDocCount();
            System.out.println("key=" + key + ",docCount=" + docCount);

            TopHits hits = bucket.getAggregations().get("top");
            for (SearchHit hit : hits.getHits().getHits()) {
                logger.info("-->id[{}],_source[{}]", hit.getId(), hit.getSourceAsString());
            }
        }
    }

    /**
     * @func 在全局上下文搜索
     * @author wangpeng
     * @date 2018/8/13 13:25
     */
    @Test
    public void globalAgg() {
        GlobalAggregationBuilder builder = AggregationBuilders.global("agg")
                .subAggregation(
                        AggregationBuilders.terms("modules").field("module")
                );
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Global agg = response.getAggregations().get("agg");
        System.out.println(agg.getDocCount());//41264
    }

    /**
     * @func 过滤出module=nova的文档
     * @author wangpeng
     * @date 2018/8/13 13:31
     */
    @Test
    public void filterAgg() {
        FilterAggregationBuilder builder = AggregationBuilders.filter(
                "agg",
                QueryBuilders.termQuery("module", "nova")
        );
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Filter agg = response.getAggregations().get("agg");
        System.out.println(agg.getDocCount());//15788
    }

    /**
     * @func 多个filter
     * @author wangpeng
     * @date 2018/8/13 13:37
     */
    @Test
    public void filtersAgg() {
        FiltersAggregationBuilder builder = AggregationBuilders.filters(
                "agg",
                new FiltersAggregator.KeyedFilter("nova", QueryBuilders.termQuery("module", "nova")),
                new FiltersAggregator.KeyedFilter("cinder", QueryBuilders.termQuery("module", "cinder"))
        );
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Filters agg = response.getAggregations().get("agg");
        for (Filters.Bucket entry : agg.getBuckets()) {
            String key = entry.getKeyAsString();
            long docCount = entry.getDocCount();
            logger.info("key:[{}],docCount:[{}]", key, docCount);
        }
        /*
        key:[cinder],docCount:[3692]
        key:[nova],docCount:[15788]
         */
    }

    /**
     * @func 查找缺失文档
     * @author wangpeng
     * @date 2018/8/13 13:46
     */
    @Test
    public void missingAgg() {
        MissingAggregationBuilder builder = AggregationBuilders.missing("agg").field("module");
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Missing agg = response.getAggregations().get("agg");
        System.out.println(agg.getDocCount());//0
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/13 13:52
     */
    @Test
    public void nestedAgg() {
        NestedAggregationBuilder builder = AggregationBuilders.nested("agg", "module");
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Nested agg = response.getAggregations().get("agg");
        System.out.println(agg.getDocCount());//0
    }

    /**
     * @func terms聚合
     * @author wangpeng
     * @date 2018/8/13 14:05
     */
    @Test
    public void termsAgg() {
        TermsAggregationBuilder builder = AggregationBuilders.terms("agg").field("module");
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Terms agg = response.getAggregations().get("agg");
        for (Terms.Bucket bucket : agg.getBuckets()) {
            logger.info("key:[{}],docCount:[{}]", bucket.getKeyAsString(), bucket.getDocCount());
        }
        /*
        key:[nova],docCount:[15788]
        key:[neutron],docCount:[9749]
        key:[keystone],docCount:[7356]
        key:[cinder],docCount:[3692]
        key:[messages],docCount:[3201]
        key:[dashboard],docCount:[947]
        key:[glance],docCount:[351]
        key:[rabbitmq],docCount:[168]
        key:[keepalived],docCount:[12]
         */
    }

    /**
     * @func 排序测试
     * @author wangpeng
     * @date 2018/8/13 14:08
     */
    @Test
    public void orderTest() {
        TermsAggregationBuilder builder = AggregationBuilders.terms("agg")
                .field("module")
                .order(Terms.Order.count(true)) /*按文档数量升序*/
//                .order(Terms.Order.term(false))/*按字母序降序*/
                ;
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Terms agg = response.getAggregations().get("agg");
        for (Terms.Bucket bucket : agg.getBuckets()) {
            logger.info("key:[{}],docCount:[{}]", bucket.getKeyAsString(), bucket.getDocCount());
        }
        /*
        key:[keepalived],docCount:[12]
        key:[rabbitmq],docCount:[168]
        key:[glance],docCount:[351]
        key:[dashboard],docCount:[947]
        key:[messages],docCount:[3201]
        key:[cinder],docCount:[3692]
        key:[keystone],docCount:[7356]
        key:[neutron],docCount:[9749]
        key:[nova],docCount:[15788]
         */
    }


    @Test
    public void significantTermsAgg() {

    }

    @Test
    public void rangeAgg() {

    }

    /**
     * @func 查询日期分段的数量
     * @author wangpeng
     * @date 2018/8/13 14:27
     */
    @Test
    public void dateRangeAgg() {
        DateRangeAggregationBuilder builder = AggregationBuilders.dateRange("agg")
                .field("logdate")
                .format("HH")
                .addUnboundedTo("12")/*-无穷-12*/
                .addRange("12", "18")/*12-18*/
                .addUnboundedFrom("18");
        SearchResponse response = client.prepareSearch(indexName).addAggregation(builder).execute().actionGet();
        Range agg = response.getAggregations().get("agg");
        for (Range.Bucket bucket : agg.getBuckets()) {
            String key = bucket.getKeyAsString();
            Object from = bucket.getFrom();
            Object to = bucket.getTo();
            long docCount = bucket.getDocCount();
            logger.info("key[{}],from[{}],to[{}],docCount[{}]", key, from, to, docCount);
        }
        /*
        key[*-12],from[null],to[1970-01-01T12:00:00.000Z],docCount[0]
        key[12-18],from[1970-01-01T12:00:00.000Z],to[1970-01-01T18:00:00.000Z],docCount[0]
        key[18-*],from[1970-01-01T18:00:00.000Z],to[null],docCount[41264]
         */
    }


}
