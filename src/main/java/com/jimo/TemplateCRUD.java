package com.jimo;

import com.carrotsearch.hppc.cursors.ObjectCursor;
import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.admin.indices.template.delete.DeleteIndexTemplateResponse;
import org.elasticsearch.action.admin.indices.template.get.GetIndexTemplatesRequest;
import org.elasticsearch.action.admin.indices.template.get.GetIndexTemplatesResponse;
import org.elasticsearch.action.admin.indices.template.put.PutIndexTemplateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.cluster.metadata.IndexTemplateMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.compress.CompressedXContent;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/16 16:49
 */
public class TemplateCRUD {
    private final Logger logger = LogManager.getLogger(IndexCRUD.class);

    private TransportClient client;

    private String indexName = "blog";
    private String type = "novel";

    /**
     * @func 初始化连接
     * @author wangpeng
     * @date 2018/8/11 21:56
     */
    @Before
    public void init() {
        try {
            client = ESAPI.getTransportClient();
        } catch (UnknownHostException e) {
            logger.error("初始化失败");
        }
    }

    /**
     * @func 创建模板
     * @author wangpeng
     * @date 2018/8/16 16:50
     */
    @Test
    public void createIndexTemplate() {
        PutIndexTemplateRequest request = new PutIndexTemplateRequest("blog");
        putSettings(request);
        putMapping(request);
        request.template("blog*");
        boolean acknowledged = client.admin().indices().putTemplate(request).actionGet().isAcknowledged();
        System.out.println(acknowledged);
    }

    /**
     * @func 设置模板mapping, type为java，定义了个name字段
     * @author wangpeng
     * @date 2018/8/16 17:02
     */
    private void putMapping(PutIndexTemplateRequest request) {
        String properties = "{\n" +
                "  \"properties\": {\n" +
                "    \"name\": {\n" +
                "      \"type\": \"text\"\n" +
                "    }\n" +
                "  }\n" +
                "}";
        request.mapping("java", properties, XContentType.JSON);
    }

    /**
     * @func 设置模板setting
     * @author wangpeng
     * @date 2018/8/16 16:57
     */
    private void putSettings(PutIndexTemplateRequest request) {
        Map<String, Object> settings = new HashMap<>();
        settings.put("number_of_shards", 1);
        request.settings(settings);
    }


    /**
     * @func 查询模板
     * @author wangpeng
     * @date 2018/8/16 17:23
     */
    @Test
    public void getTemplate() throws IOException {
        GetIndexTemplatesRequest request = new GetIndexTemplatesRequest("blog");
        GetIndexTemplatesResponse response = client.admin().indices().getTemplates(request).actionGet();
        List<IndexTemplateMetaData> indexTemplates = response.getIndexTemplates();
        for (IndexTemplateMetaData t : indexTemplates) {
            String template = t.getTemplate();
            System.out.println(template);
            //print mappings
            ImmutableOpenMap<String, CompressedXContent> mappings = t.getMappings();
            for (ObjectCursor<String> type : mappings.keys()) {
                CompressedXContent compressedXContent = mappings.get(type.value);
                System.out.println(compressedXContent.string());
            }
            //print settings
            Settings settings = t.getSettings();
            for (String key : settings.keySet()) {
                System.out.println(key + ":" + settings.get(key));
            }
        }
        /*
        blog*
        {
          "properties": {
            "name": {
              "type": "text"
            }
          }
        }
        index.number_of_shards:1
         */
    }

    /**
     * @func 删除模板
     * @author wangpeng
     * @date 2018/8/16 17:27
     */
    @Test
    public void deleteTemplate() {
        DeleteIndexTemplateResponse response = client.admin().indices()
                .prepareDeleteTemplate("blog")
                .execute().actionGet();
        System.out.println(response.isAcknowledged());
    }
}
