package com.jimo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequestBuilder;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class ESAPI {

    private final Logger logger = LogManager.getLogger(ESAPI.class);
    private TransportClient client;

    public ESAPI() {
        try {
            this.client = getTransportClient();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * @func 获取实例
     * @author wangpeng
     * @date 2018/8/9 10:17
     */
    public static TransportClient getTransportClient() throws UnknownHostException {
        String[] hosts = "hadoop4:9300,hadoop5:9300,hadoop6:9300,hadoop7:9300,hadoop8:9300,hadoop9:9300".split(",");
        Settings settings = Settings.builder().put("cluster.name", "sinorail").build();
        PreBuiltTransportClient client = new PreBuiltTransportClient(settings);

        for (String host : hosts) {
            String ip = host.split(":")[0];
            String port = host.split(":")[1];
            client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(ip), Integer.parseInt(port)));
        }
        return client;
    }

    void insertByJson(String json, String indexName, String type) {
        IndexResponse response = client.prepareIndex(indexName, type)
                .setSource(json, XContentType.JSON)
                .get();
        logger.info("插入JSON ok,版本：" + response.getVersion());
    }

    void insertByMap(Map<String, Object> map, String indexName, String type) {
        IndexResponse response = client.prepareIndex(indexName, type)
                .setSource(map).get();
        logger.info("插入Map ok,版本：" + response.getVersion());
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/9 10:55
     */
    GetResponse getDocById(String indexName, String type, String id) {
        GetResponse response = client.prepareGet(indexName, type, id)
                .setOperationThreaded(true)
                .get();
        logger.info("getDocById ok,Source：" + response.getSourceAsString());
        return response;
    }

    void deleteDocById(String indexName, String type, String id) {
        DeleteResponse response = client.prepareDelete(indexName, type, id).get();
        logger.info("delete ok," + response.getResult().toString());
    }

    /**
     * @func 同步查询删除
     * @author wangpeng
     * @date 2018/8/9 11:04
     */
    void deleteByQuerySync(String name, String text, String... indices) {
        BulkByScrollResponse response = DeleteByQueryAction.INSTANCE.newRequestBuilder(client)
                .filter(QueryBuilders.matchQuery(name, text))
                .source(indices)
                .get();
        logger.info("delete by query sync ok,删除了" + response.getDeleted());
    }

    /**
     * @func 异步删除，用于比较耗时的
     * @author wangpeng
     * @date 2018/8/9 11:09
     */
    void deleteByQueryAsync(String name, String text, String... indices) {
        DeleteByQueryAction.INSTANCE.newRequestBuilder(client)
                .filter(QueryBuilders.matchQuery(name, text))
                .source(indices)
                .execute(
                        new ActionListener<BulkByScrollResponse>() {
                            public void onResponse(BulkByScrollResponse bulkByScrollResponse) {
                                logger.info("delete by query async ok," + bulkByScrollResponse.getDeleted());

                            }

                            public void onFailure(Exception e) {
                                logger.error("delete failure");
                            }
                        }
                );
    }

    void updateById(String indexName, String type, String id, String json) throws ExecutionException, InterruptedException {
        UpdateRequest updateRequest = new UpdateRequest(indexName, type, id);
        updateRequest.doc(json, XContentType.JSON);
        UpdateResponse response = client.update(updateRequest).get();
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/9 11:22
     */
    void updateDocById(String index, String type, String id, Map<String, Object> map) {
        UpdateResponse response = client.prepareUpdate(index, type, id).setDoc(map).get();
        logger.info("update ok," + response.getVersion());
    }

    void multiGetByIds(String index, String type, String... ids) {
        MultiGetRequestBuilder multiGetRequestBuilder = client.prepareMultiGet();
        for (String id : ids) {
            multiGetRequestBuilder.add(index, type, id);
        }
        MultiGetResponse itemResponses = multiGetRequestBuilder.get();
        for (MultiGetItemResponse response : itemResponses) {
            GetResponse r = response.getResponse();
            if (r.isExists()) {
                logger.info("json: " + r.getSourceAsString());
            }
        }
    }

    /**
     * @func 批量插入json字符串数据
     * @author wangpeng
     * @date 2018/8/9 11:45
     */
    void bulkInsertByJsonList(String index, String type, List<String> jsonList) {
        BulkRequestBuilder bulk = client.prepareBulk();
        jsonList.forEach(json -> bulk.add(client.prepareIndex(index, type).setSource(json, XContentType.JSON)));
        BulkResponse responses = bulk.get();
        if (responses.hasFailures()) {
            logger.error("bulk insert error");
        } else {
            logger.info("bulk insert ok" + responses.getTookInMillis());
        }
    }


    void aggs(String index, String field, String field2) throws IOException {
        SearchResponse searchResponse = client.prepareSearch(index)
                .setQuery(QueryBuilders.matchAllQuery())
                .addAggregation(AggregationBuilders.terms("jimo").field(field))
                .addAggregation(
                        AggregationBuilders.dateHistogram("dates").field(field2)
                                .dateHistogramInterval(DateHistogramInterval.YEAR))
                .get();
        Terms jimo = searchResponse.getAggregations().get("jimo");
        Histogram dates = searchResponse.getAggregations().get("dates");
        System.out.println("type:" + jimo.getType());
        System.out.println(dates.getType());
    }
}
