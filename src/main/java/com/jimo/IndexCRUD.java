package com.jimo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

public class IndexCRUD {
    private final Logger logger = LogManager.getLogger(IndexCRUD.class);

    private TransportClient client;

    private String indexName = "blog";
    private String type = "novel";

    /**
     * @func 初始化连接
     * @author wangpeng
     * @date 2018/8/11 21:56
     */
    @Before
    public void init() {
        try {
            client = ESAPI.getTransportClient();
        } catch (UnknownHostException e) {
            logger.error("初始化失败");
        }
    }

    /**
     * @func 判断index是否存在
     * @author wangpeng
     * @date 2018/8/11 22:09
     */
    private boolean indexExist(String index) {
        return client.admin().indices().prepareExists(index).execute().actionGet().isExists();
    }

    /**
     * @func 创建索引，默认配置
     * @author wangpeng
     * @date 2018/8/12 8:34
     */
    @Test
    public void createIndex() {
        if (indexExist(indexName)) {
            deleteIndex();
        }
        CreateIndexResponse response = client.admin().indices().prepareCreate(indexName).get();
        if (response.isAcknowledged()) {
            logger.info("create index ok");
        } else {
            logger.error("create index failed");
        }
    }

    /*https://stackoverflow.com/questions/22071198/adding-mapping-to-a-type-from-java-how-do-i-do-it
    * https://stackoverflow.com/questions/42254823/elasticsearch-java-api-set-index-settings
    * */

    /**
     * @func 创建时指定mapping和setting
     * @author wangpeng
     * @date 2018/8/12 8:34
     */
    @Test
    public void createIndexWithMapping() {
        //mapping
        String jsonMapping = "{" +
                "\"novel\":{" +
                "\"properties\":{" +
                "\"title\":{" +
                "\"type\":" +
                "\"string\"}}}}";
        CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate(indexName);
        createIndexRequestBuilder.addMapping(type, jsonMapping, XContentType.JSON);
        //setting
        String settingJson = "{\"number_of_shards\":1}";
        createIndexRequestBuilder.setSettings(settingJson, XContentType.JSON);
        CreateIndexResponse response = createIndexRequestBuilder.execute().actionGet();
        if (response.isAcknowledged()) {
            logger.info("create index ok");
        } else {
            logger.error("create index failed");
        }
    }

    /*https://stackoverflow.com/questions/33134906/elasticsearch-find-all-indexes-using-the-java-client*/

    /**
     * @func 获取所有索引
     * @author wangpeng
     * @date 2018/8/12 8:34
     */
    @Test
    public void getIndex() throws ExecutionException, InterruptedException, IOException {
//        ImmutableOpenMap<String, IndexMetaData> datas = client.admin().cluster().prepareState().get().getState().getMetaData().getIndices();
        String[] indices = client.admin().indices().getIndex(new GetIndexRequest()).actionGet().getIndices();
        for (String indice : indices) {
            System.out.println(indice);
        }
        //way 2
//        GetIndexResponse response = client.admin().indices().prepareGetIndex().addIndices(indexName).get();
//        System.out.println(response.getIndices().length);
    }


    @Test
    public void updateIndex() {
        //update mapping

        //update setting
    }

    /**
     * @func 删除索引
     * @author wangpeng
     * @date 2018/8/12 8:35
     */
    @Test
    public void deleteIndex() {
        DeleteIndexResponse response = client.admin().indices().prepareDelete(indexName).execute().actionGet();
        if (response.isAcknowledged()) {
            logger.info("delete index ok");
        } else {
            logger.error("delete index failed");
        }
    }
}
