package com.jimo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.rest.RestStatus;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

public class SingleDocumentCRUD {
    private final Logger logger = LogManager.getLogger(SingleDocumentCRUD.class);

    private TransportClient client;

    private String indexName = "blog";
    private String type = "novel";

    /**
     * @func 初始化连接
     * @author wangpeng
     * @date 2018/8/11 21:56
     */
    @Before
    public void init() {
        try {
            client = ESAPI.getTransportClient();
        } catch (UnknownHostException e) {
            logger.error("初始化失败");
        }
    }

    /**
     * @func 插入一条文档
     * @author wangpeng
     * @date 2018/8/12 8:48
     */
    @Test
    public void insertDoc() throws IOException {
        XContentBuilder xContentBuilder = jsonBuilder().startObject()
                .field("title", "西游记")
                .endObject();
        IndexResponse response = client.prepareIndex(indexName, type, "1")
                .setSource(xContentBuilder).get();
        if (response.status().equals(RestStatus.OK)) {
            logger.info("insert doc ok");
        } else {
            logger.error("insert doc error");
        }
    }

    /**
     * @func 取得一条数据
     * @author wangpeng
     * @date 2018/8/12 8:57
     */
    @Test
    public void getDoc() {
        GetResponse response = client.prepareGet(indexName, type, "1").get();
        if (response.isExists()) {
            logger.info(response.getSourceAsString());/*{"title":"西游记"}*/
        } else {
            logger.error("get doc failed");
        }
    }

    /**
     * @func 更新一条数据, 如果没有则插入
     * @author wangpeng
     * @date 2018/8/12 9:00
     */
    @Test
    public void updateDoc() throws IOException, ExecutionException, InterruptedException {
        IndexRequest indexRequest = new IndexRequest(indexName, type, "3")
                .source(jsonBuilder().startObject()
                        .field("title", "红楼梦")
                        .endObject());
        UpdateRequest updateRequest = new UpdateRequest(indexName, type, "3")
                .doc(jsonBuilder().startObject()
                        .field("title", "红楼梦")
                        .endObject())
                .upsert(indexRequest);
        UpdateResponse response = client.update(updateRequest).get();
        System.out.println(response.status());
        if (response.status().equals(RestStatus.OK)) {
            logger.info("update ok");
        } else if (response.status().equals(RestStatus.CREATED)) {
            logger.info("不存在，但插入成功了");
        } else {
            logger.error("update failed: ");
        }
    }

    /**
     * @func 删除一条记录
     * @author wangpeng
     * @date 2018/8/12 9:08
     */
    @Test
    public void deleteDoc() {
        DeleteResponse response = client.prepareDelete(indexName, type, "2").get();
        System.out.println(response.status());
        if (response.status().equals(RestStatus.OK)) {
            logger.info("delete ok");
        } else {
            logger.error("delete failed");
        }
    }

}
