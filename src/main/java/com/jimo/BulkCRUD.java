package com.jimo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.*;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.junit.Before;
import org.junit.Test;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BulkCRUD {
    private final Logger logger = LogManager.getLogger(BulkCRUD.class);

    private TransportClient client;

    private String indexName = "blog";
    private String type = "novel";

    /**
     * @func 初始化连接
     * @author wangpeng
     * @date 2018/8/11 21:56
     */
    @Before
    public void init() {
        try {
            client = ESAPI.getTransportClient();
        } catch (UnknownHostException e) {
            logger.error("初始化失败");
        }
    }

    /**
     * @func 批量插入
     * @author wangpeng
     * @date 2018/8/12 9:15
     */
    @Test
    public void bulkInsert() {
        List<String> jsonDatas = Arrays.asList("{\"title\":\"Java入门\"}", "{\"title\":\"寂寞的奇幻旅程\"}");
        BulkRequestBuilder bulkRequestBuilder = client.prepareBulk();
        jsonDatas.forEach(json -> bulkRequestBuilder.add(client.prepareIndex(indexName, type).setSource(json, XContentType.JSON)));
        BulkResponse responses = bulkRequestBuilder.get();
        System.out.println(responses.status());/*OK*/
        if (responses.hasFailures()) {
            logger.error(responses.buildFailureMessage());
        } else {
            logger.info("bulk insert ok");
        }
    }

    /**
     * @func 批量查询
     * @author wangpeng
     * @date 2018/8/12 9:24
     */
    @Test
    public void multiGet() {
        MultiGetResponse responses = client.prepareMultiGet()
                .add(indexName, type, "1")
                .add(indexName, type, "2", "3", "AWUru6pWNX6v9z7qdrdM", "AWUru6pWNX6v9z7qdrdN")
                .get();
        for (MultiGetItemResponse itemResponse : responses) {
            GetResponse r = itemResponse.getResponse();
            if (r.isExists()) {
                logger.info(r.getSourceAsString());
            }
        }
        /*
         {"title":"西游记"}
         {"title":"红楼梦"}
         {"title":"Java进阶"}
         {"title":"寂寞的奇幻旅程"}
         */
    }

    /**
     * @func 批量更新
     * @author wangpeng
     * @date 2018/8/12 9:29
     */
    @Test
    public void bulkUpdate() {
        UpdateByQueryRequestBuilder updateBuQuery = UpdateByQueryAction.INSTANCE.newRequestBuilder(client);
        updateBuQuery.source(indexName)
                .filter(QueryBuilders.matchQuery("title", "java"))
                .size(10)
                .script(
                        new Script(ScriptType.INLINE, "ctx._source.title = \"Java太好了\"", "painless", Collections.emptyMap())
                );
        BulkByScrollResponse response = updateBuQuery.get();
        System.out.println(response.getStatus());
        if (response.getStatus().equals(RestStatus.OK)) {
            logger.info("bulk update ok: " + response.getUpdated());
        } else {
            logger.error("failure: " + response.getBulkFailures().size());
        }
    }

    /**
     * @func 批量删除：delete by query
     * @author wangpeng
     * @date 2018/8/12 10:17
     */
    @Test
    public void bulkDelete() {
        BulkByScrollResponse response =
                DeleteByQueryAction.INSTANCE.newRequestBuilder(client)
                        .filter(QueryBuilders.matchQuery("title", "java"))
                        .source(indexName)
                        .get();
        logger.info("deleted: " + response.getDeleted());/*deleted: 1*/
    }
}
