package com.jimo;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/9 10:36
 */
public class TestESAPI {

    private ESAPI esapi;
    private String indexName = "twitter";
    private String type = "tweet";

    @Before
    public void init() throws Exception {
        esapi = new ESAPI();
    }

    @Test
    public void insertByJson() throws Exception {
        String json = "{" +
                "\"user\":\"kimchy\"," +
                "\"postDate\":\"2013-01-30\"," +
                "\"message\":\"trying out Elasticsearch\"" +
                "}";
        esapi.insertByJson(json, indexName, type);
    }

    @Test
    public void insertByMap() throws Exception {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("user", "kimchy");
        json.put("postDate", new Date());
        json.put("message", "trying out Elasticsearch");
        esapi.insertByMap(json, indexName, type);
    }

    @Test
    public void getById() throws Exception {
        esapi.getDocById(indexName, type, "1");
        /*getDocById ok,Source：{"user":"韩立","postDate":"2018-08-08T05:53:59.350Z",
        "message":"Hello Elasticsearch and fanrenxiuxian","gender":"male"}*/
    }

    @Test
    public void deleteById() throws Exception {
        esapi.deleteDocById(indexName, type, "AWUcj9dogCi6VZ1OLCo2");
        /*delete ok,DELETED*/
    }

    @Test
    public void deleteByQuerySync() throws Exception {
        esapi.deleteByQuerySync("user", "kimchy", indexName);
        /*delete by query sync ok,2 */
    }

    @Test
    public void deleteByQueryAsync() throws Exception {
        esapi.deleteByQueryAsync("user", "kk", indexName);
    }

    @Test
    public void updateDocById() throws Exception {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("user", "jimo");
        esapi.updateDocById(indexName, type, "AWUcqrTbgCi6VZ1OLFLW", json);
    }

    @Test
    public void multiGetByIds() throws Exception {
        esapi.multiGetByIds(indexName, type, "1", "2", "4");
        /*
        * json: {"user":"韩立","postDate":"2018-08-08T05:53:59.350Z","message":"Hello Elasticsearch and fanrenxiuxian","gender":"male"}
          json: {"user":"梦入神机","postDate":"2018-08-08T07:11:52.141Z","message":"Hello Elasticsearch and 星河大帝"}
        * */
    }

    @Test
    public void bulkInsertByJsonList() throws Exception {
        String json = "{" +
                "\"user\":\"kimchy\"," +
                "\"postDate\":\"2013-01-30\"," +
                "\"message\":\"trying out Elasticsearch\"" +
                "}";
        List<String> jsons = Arrays.asList(json, json);
        esapi.bulkInsertByJsonList(indexName, type, jsons);
    }

    @Test
    public void aggs() throws Exception {
        esapi.aggs("20180807-openstack_log", "module", "logdate");
    }
}
